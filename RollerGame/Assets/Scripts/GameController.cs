﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

//EVENT
    public delegate void GameControllerDelegate();
    public static event GameControllerDelegate OnTrigGameStarted;
    public static event GameControllerDelegate OnTrigGameReplay;

    public static GameController Instance;

//Worlds
    [SerializeField]
    private GameObject playerObj;

    [SerializeField]
    private GameObject spawnResource;

    //Text
    [SerializeField]
    private Text txtGameName;

    [SerializeField]
    private Text txtHighScore;

    [SerializeField]
    private Text txtScoreInPlayGame;

    [SerializeField]
    private Text txtScoreGameOver;

    [SerializeField]
    private Text txtGameOver;

    [SerializeField]
    private Text txtPauseGame;

//UI Page
    [SerializeField]
    private GameObject menuPage;

    [SerializeField]
    private GameObject playPage;

    [SerializeField]
    private GameObject gameOverPage;

    [SerializeField]
    private GameObject pausePage;

//Buttons
    [SerializeField]
    private GameObject playButton;

    [SerializeField]
    private GameObject exitButton;

    [SerializeField]
    private GameObject rePlayButton;

    [SerializeField]
    private GameObject pauseButton;

    [SerializeField]
    private GameObject resumeButton;

    //Black Background: enable when state is GameOver or popup Pause Game is enable
    [SerializeField]
    private GameObject darkBackground;

    enum PageState
    {
        Menu,
        Play,
        Pause,
        GameOver
    }
    private PageState pageState;

    int score = 0;

    bool gameOver = false;
    public bool GameOver
    {
        get { return gameOver; }
        set { gameOver = value; }
    }

    bool pauseGame = false;
    public bool PauseGame
    {
        get { return pauseGame; }
        set { pauseGame = value; }
    }

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        SetPageState(PageState.Menu);
    }

    private void Reset()
    {
        score = 0;
        gameOver = false;
        pauseGame = false;
        txtScoreInPlayGame.text = "0";
    }

    private void OnEnable()
    {
        PlayerController.OnTrigPlayerScored += OnTrigPlayerScored;
        PlayerController.OnTrigPlayerDied += OnTrigPlayerDied;
    }

    private void OnDisable()
    {
        PlayerController.OnTrigPlayerScored -= OnTrigPlayerScored;
        PlayerController.OnTrigPlayerDied -= OnTrigPlayerDied;
    }

    void SetPageState(PageState state)
    {
        playerObj.SetActive(state != PageState.Menu);
        spawnResource.SetActive(state != PageState.Menu);
        switch (state)
        {
            case PageState.Menu:
                int highestScore = PlayerPrefs.GetInt(ConstantManager.KEY_HIGH_SCORE_OF_ROLLER_GAME);
                //@TODO: can get user's score from SERVER
                txtHighScore.text = ConstantManager.TEXT_HIGH_SCORE + highestScore.ToString();
                txtGameName.text = ConstantManager.GAME_NAME;
                menuPage.SetActive(true);
                playPage.SetActive(false);
                gameOverPage.SetActive(false);
                pausePage.SetActive(false);
                darkBackground.gameObject.SetActive(false);
                break;

            case PageState.Play:
                menuPage.SetActive(false);
                playPage.SetActive(true);
                gameOverPage.SetActive(false);
                pausePage.SetActive(false);
                darkBackground.gameObject.SetActive(false);
                break;

            case PageState.GameOver:
                txtGameOver.text = ConstantManager.TXT_GAME_OVER;
                txtScoreGameOver.text = ConstantManager.TEXT_FINAL_SCORE + score.ToString();
                menuPage.SetActive(false);
                playPage.SetActive(false);
                gameOverPage.SetActive(true);
                pausePage.SetActive(false);
                darkBackground.gameObject.SetActive(true);
                break;

            case PageState.Pause:
                txtPauseGame.text = ConstantManager.TXT_PAUSE_GAME;
                menuPage.SetActive(false);
                playPage.SetActive(false);
                gameOverPage.SetActive(false);
                pausePage.SetActive(true);
                darkBackground.gameObject.SetActive(true);
                break;
                
            default:
                break;
        }
        playButton.SetActive(state == PageState.Menu);
        pauseButton.gameObject.SetActive(state == PageState.Play);
        rePlayButton.SetActive(state == PageState.GameOver);
        resumeButton.SetActive(state == PageState.Pause);
        exitButton.SetActive(state == PageState.Menu || state == PageState.GameOver || state == PageState.Pause);
    }

//Function of buttons
    public void OnStartGame()
    {
        //actived when play button is hit
        Reset();
        SetPageState(PageState.Play);
        OnTrigGameStarted();
    }

    public void OnExitGame()
    {
        //actived when exit button is hit
        //@todo: exit game
        //OnTrigGameExit();
        PlayerPrefs.Save();
        Application.Quit();
    }

    public void OnPauseGame()
    {
        //actived when pause button is hit
        SetPageState(PageState.Pause);
        PauseGame = true;
    }

    public void OnResumeGame()
    {
        //actived when resume button is hit
        //@todo:
        SetPageState(PageState.Play);
        PauseGame = false;
    }

    public void OnReplayGame()
    {
        //actived when replay button is hit
        OnTrigGameReplay();
        Reset();
        SetPageState(PageState.Play);
    }

//Events
    void OnTrigPlayerScored()
    {
        score += ConstantManager.GAIN_SCORE;
        txtScoreInPlayGame.text = score.ToString();
    }

    void OnTrigPlayerDied()
    {
        gameOver = true;
        int saveScore = PlayerPrefs.GetInt(ConstantManager.KEY_HIGH_SCORE_OF_ROLLER_GAME);
        if (score > saveScore)
        {
            //save to local
            PlayerPrefs.SetInt(ConstantManager.KEY_HIGH_SCORE_OF_ROLLER_GAME, score);
        }
        //Send to server http://ip:port/leaderboard
        OnSendToServerUpdateScore("some-user-name", score);
        SetPageState(PageState.GameOver);
    }

    //SEND DATA TO SERVER
    void OnSendToServerUpdateScore(string userName, int score)
    {
        if (string.IsNullOrEmpty(userName) || score < 0)
            return;
        //send body to update
        string body = "{\"userName\":\"" + userName + "\",\"score\":" + score + "}";
        byte[] byteBody = System.Text.Encoding.UTF8.GetBytes(body);  
        
        //Send HTTP REST
        string url = "http://" + ConstantManager.IP + ":" + ConstantManager.PORT + "/leaderboard";
        WWW www = new WWW(url, byteBody);
        StartCoroutine(WaitForRequest(www));
    }

    //Wait for the www Request
    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;
        //handle response from server
        switch (www.error)
        {
            case "404":
                //TODO: handle Username not found (user has not registered with the leaderboard service)
                break;

            case "405":
                //TODO: handle Invalid Username supplied
                break;

            case "200":
                //TODO: handle success
                break;

            default:
                //do nothing
                break;
        }
    }
}
