﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonScalerEffect : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private int autoLoop;
    private int countLoop;
    public Vector3 scaleBegin;
    public float newScaleTarget = 1.1f;
    private float defaultScaleTime = 0.3f;

    // Use this for initialization
    void Start()
    {
        scaleBegin = this.transform.localScale;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        this.transform.localScale = scaleBegin * newScaleTarget;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        transform.localScale = scaleBegin * newScaleTarget;
        transform.localScale = Vector3.Lerp(transform.localScale, scaleBegin, defaultScaleTime);
    }

    void PlayScaleEffect()
    {
        //create effect when gameobject is hit
        transform.localScale = scaleBegin * newScaleTarget;
        transform.localScale = Vector3.Lerp(transform.localScale, scaleBegin, defaultScaleTime);
        if (countLoop < autoLoop)
            Invoke("PlayScaleEffect", defaultScaleTime);
        countLoop++;
    }
}
