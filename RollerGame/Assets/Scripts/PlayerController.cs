﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

//Events
    public delegate void PlayerDelegate();
    public static event PlayerDelegate OnTrigPlayerDied;
    public static event PlayerDelegate OnTrigPlayerScored;

    [SerializeField]
    private Rigidbody2D rigidbody;

    [SerializeField]
    private float tapForce = 10;

    [SerializeField]
    public float rotationspeed = 10f;

    //SOUND
    [SerializeField]
    private AudioSource tapAudio;

    [SerializeField]
    private AudioSource scoreAudio;

    [SerializeField]
    private AudioSource gameOverAudio;

    private int maximumNumTouch = 1;// 1 or 2: user can define that num can touch when player jump
    private int currentNumTouch;
    private Vector3 startPos;
    GameController gameController;

    // Use this for initialization
    void Start () {
        if (rigidbody == null)
            rigidbody = GetComponent<Rigidbody2D>();
        if (gameController == null)
            gameController = GameController.Instance;
        startPos = this.transform.position;
    }

    private void OnEnable()
    {
        GameController.OnTrigGameStarted += OnTrigGameStarted;
        GameController.OnTrigGameReplay += OnTrigGameReplay;
    }

    private void OnDisable()
    {
        GameController.OnTrigGameStarted -= OnTrigGameStarted;
        GameController.OnTrigGameReplay -= OnTrigGameReplay;
    }


    // Update is called once per frame
    void FixedUpdate() {
        if (!rigidbody.simulated || (gameController != null && (gameController.GameOver || gameController.PauseGame)))
            return;
        if (Input.GetMouseButtonDown(0) && currentNumTouch < maximumNumTouch)
        {
            tapAudio.Play();
            rigidbody.velocity = Vector2.zero;
            rigidbody.AddForce(Vector2.up * tapForce);
            currentNumTouch++;
        }
        this.transform.Rotate(0, 0, -rotationspeed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        switch (collider.gameObject.tag)
        {
            case ConstantManager.TAG_DEAD_ZONE:
                //actived when player collide with an obstacle
                if (gameController != null && gameController.PauseGame)
                    return;
                gameOverAudio.Play();
                OnTrigPlayerDied();
                rigidbody.simulated = false;
                EnablePlayer(false);
                break;

            case ConstantManager.TAG_SCORE_ZONE:
                //actived when player jump over an obstacle  
                scoreAudio.Play();
                OnTrigPlayerScored();
                break;

            case ConstantManager.TAG_GROUND_ZONE:
                //actived when player on the ground
                currentNumTouch = 0;
                break;

            default:
                break;
        }
    }

//Events

    void OnTrigGameStarted()
    {
        //active when game started
        rigidbody.velocity = Vector3.zero;
        rigidbody.simulated = true;
        EnablePlayer(true);
    }

    void OnTrigGameReplay()
    {
        //active when game replay
        transform.position = startPos;
        rigidbody.simulated = true;
        EnablePlayer(true);
    }

    void EnablePlayer(bool isEnable)
    {
        //Show/hide player
        SpriteRenderer sRenderer = gameObject.GetComponent<SpriteRenderer>();
        if (sRenderer != null)
        {
            Color cLor = sRenderer.color;
            cLor.a = (isEnable) ? 1 : 0;
            sRenderer.color = cLor;
        }
    }
}
