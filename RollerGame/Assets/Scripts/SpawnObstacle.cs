﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObstacle : MonoBehaviour {

    class PoolObject
    {
        public Transform trans;
        public bool inUse;
        public PoolObject(Transform t)
        {
            trans = t;
        }

        public void Use()
        {
            inUse = true;
        }

        public void Dispose()
        {
            inUse = false;
        }
    }

    [SerializeField]
    private GameObject objPrefab;

    [SerializeField]
    private int poolSize;

    [SerializeField]
    private int spaceMultiX = 3;

    int initPos = 1000;

    [SerializeField]
    private float shiftSpeed;

    [SerializeField]
    private float spawnRate;

    private float spawnTimer;

    public List<Material> materialRange;

    [System.Serializable]
    public struct ScaleSpawnRange
    {
        public float min;
        public float max;
    }
    public ScaleSpawnRange scaleSpawnRange;

//Cloud is spawn immediate
    public bool spawnImmediate;//particle prewarm
    public Vector3 immediateSpawnPos;
    PoolObject[] poolObjects;
    bool isGameStarted;
    GameController gameController;


    private void Awake()
    {
        OnConfig();
    }

    private void OnEnable()
    {
        if (gameController == null)
            gameController = GameController.Instance;
        GameController.OnTrigGameStarted += OnTrigGameStarted;
        PlayerController.OnTrigPlayerDied += OnTrigPlayerDied;
    }

    private void OnDisable()
    {
        GameController.OnTrigGameStarted -= OnTrigGameStarted;
        PlayerController.OnTrigPlayerDied -= OnTrigPlayerDied;
    }

    private void Update()
    {
        if (!isGameStarted || (gameController != null && (gameController.GameOver || gameController.PauseGame)))
            return;

        OnShift();
        spawnTimer += Time.deltaTime;
        if (spawnTimer > spawnRate)
        {
            OnSpawn();
            spawnTimer = 0;
        }
    }

//Pools
    void OnConfig()
    {
        //Create Pool
        isGameStarted = false;
        if (poolSize <= 0)
            return;
        poolObjects = new PoolObject[poolSize];
        for (int i = 0; i < poolObjects.Length; i++)
        {
            Vector3 scale = objPrefab.transform.localScale;
            GameObject go = Instantiate(objPrefab) as GameObject;
            Transform t = go.transform;
            t.SetParent(transform);
            t.localScale = scale;            
            t.position = new Vector3(initPos, 0, 0);
            poolObjects[i] = new PoolObject(t);
        }
        if (spawnImmediate)
        {
            OnSpawnImmediate();
        }
    }

    void OnSpawn()
    {
        //Get object from Pool and show on Screen
        Transform t = GetPoolObject();
        if (t == null)
            return;
        //Position
        Vector3 pos = Vector3.zero;
        pos.x = Camera.main.aspect * spaceMultiX / 2;//obsacle is appeared on screen right
        t.localPosition = pos;
        OnSetScaleAndMaterial(t);
    }

    void OnSpawnImmediate()
    {
        //Get object from Pool and show on Screen
        Transform t = GetPoolObject();
        if (t == null)
            return;
        //Position
        Vector3 pos = Vector3.zero;
        pos.x = immediateSpawnPos.x;
        t.localPosition = pos;
        OnSetScaleAndMaterial(t);
        OnSpawn();
    }

    void OnSetScaleAndMaterial(Transform t)
    {
        if (t == null)
            return;
        //Scale
        float sRandom = Random.Range(scaleSpawnRange.min, scaleSpawnRange.max);
        Vector3 scale = t.transform.localScale;
        scale.x = sRandom;
        scale.y = sRandom;
        t.transform.localScale = scale;

        //Material
        if (materialRange != null && materialRange.Count > 0)
        {
            Material matRandom = materialRange[Random.Range(0, materialRange.Count)];
            SpriteRenderer[] spriteRenders = t.gameObject.GetComponentsInChildren<SpriteRenderer>(true);
            if (spriteRenders != null && spriteRenders.Length > 0)
            {
                for (int i = 0, len = spriteRenders.Length; i < len; i++)
                {
                    spriteRenders[i].material = matRandom;
                }
            }
        }
    }

    void OnShift()
    {
        //Move object of Pool to Left
        if (poolObjects == null || poolObjects.Length <= 0)
            return;
        int len = poolObjects.Length;
        PoolObject poolObj;
        for (int i = 0; i < len; i++)
        {
            poolObj = poolObjects[i];
            if (poolObj == null)
                continue;
            poolObj.trans.position += -Vector3.right * shiftSpeed * Time.deltaTime;
            CheckDisposeObject(poolObj);
        }
    }

    void CheckDisposeObject(PoolObject poolObject)
    {
        if (poolObject == null)
            return;
        //Add object to Pool
        if (poolObject.trans.position.x < -(Camera.main.aspect * spaceMultiX))
        {
            poolObject.Dispose();
            poolObject.trans.position = Vector3.one * initPos;
        }
    }

    Transform GetPoolObject()
    {
        //Get object of Pool
        if (poolObjects == null || poolObjects.Length <= 0)
            return null;
        PoolObject poolObj = null;
        for (int i = 0; i < poolObjects.Length; i++)
        {
            poolObj = poolObjects[i];
            if (!poolObj.inUse)
            {
                poolObj.Use();
                return poolObj.trans;
            }
        }
        return null;
    }

//Events
    void OnTrigGameStarted()
    {
        isGameStarted = true;
    }

    void OnTrigPlayerDied()
    {
        if (poolObjects == null || poolObjects.Length <= 0)
            return;
        for (int i = 0; i < poolObjects.Length; i++)
        {
            poolObjects[i].Dispose();
            poolObjects[i].trans.position = Vector3.one * 1000;
        }
        if (spawnImmediate)
            OnSpawnImmediate();
    }
}
