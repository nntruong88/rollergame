﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantManager : MonoBehaviour {

    public static ConstantManager Instance;
    public const int GAIN_SCORE = 10;//Design

    public const string IP = "127.0.0.1";
    public const int PORT = 8080;

    public const string GAME_NAME = "ROLLER GAME";
    public const string TXT_GAME_OVER = "GAME OVER";
    public const string TXT_PAUSE_GAME = "PAUSED";
    public const string TEXT_HIGH_SCORE = "High Score:";
    public const string TEXT_FINAL_SCORE = "Final Score:";

    public const string TAG_DEAD_ZONE = "DeadZone";
    public const string TAG_SCORE_ZONE = "ScoreZone";
    public const string TAG_GROUND_ZONE = "GroundZone";

    public const string KEY_HIGH_SCORE_OF_ROLLER_GAME = "HighScoreRollerGame";

    private void Awake()
    {
        Instance = this;
    }
}
