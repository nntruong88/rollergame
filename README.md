# My name is Roller. It is 2D game with a player controls a rolling circle jumping over square obstacles and gain scores.

# Tools to developed this game:
	- Unity version 2018.1.2f1
	- Visual studio 2017
	- Photoshop CS6: I used it to create Background, Land, Clouds
	- Icons of https://www.flaticon.com/
	- Sounds of https://www.bfxr.net/
# Game play
	- UI Menu is display when you launch game. In this UI, you can see:
		+ Game Name
		+ High score
		+ Play button
		+ Exit button: to quit game
	- When the Play button is touch, a Player - ball - will controls a rolling ball jumping over square obstacles. 
		+ If the player jump over the obstacle, the player will gain 10 scores.
		+ If the player collide with an obstacle, UI Game Over will show.
	- UI Game Over, you can see:
		+ The information of user can send to Server
		+ Final score
		+ Replay button: you can play game again
		+ Exit button: to quit game
	- While the player is rolling, you can touch Pause button to pause game.
	- In UI Pause game, you can see:
		+ Resume button: to play game continue
		+ Exit button: to quit game
# Control
	- Player: in PlayerController script you can control:
		+ Sounds
		+ Rolling speed
		+ Rotation speed
	- Clouds and Obstacles: in SpawnObstacle script you can control:
		+ Pool size
		+ Spawn rate
		+ Moving speed
		+ Min size
		+ Max size